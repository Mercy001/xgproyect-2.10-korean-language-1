<?php

//general
$lang['index']				= '처음';
$lang['register']			= '회원가입하기';
$lang['forum']				= '게시판';

/* ------------------------------------------------------------------------------------------ */

//index.php
//case lostpassword
$lang['mail_not_exist'] 		= '이메일 주소가 존재하지 않습니다!';
$lang['mail_title']			= '새로운 비밀번호';
$lang['mail_text']			= '새로운 비밀번호를 입력해주세요: ';
$lang['mail_sended']			= '성공적으로 새로운 비밀번호로 바뀌었습니다!';

//case default
$lang['login_error']			= '이름 또는 비밀번호가 잘못되었습니다! <br /><a href="index.php" target="_top">처음으로 돌아가기</a>';

/* ------------------------------------------------------------------------------------------ */

//lostpassword.tpl
$lang['lost_pass_title']		= '비밀번호 찾기';
$lang['lost_pass_text'] 		= '이런! 비밀번호를 잊어버리셨군요! 가입할 때 기입하셨던 이메일 주소를 입력하면 새로운 비밀번호를 설정할 수 있습니다.';
$lang['retrieve_pass']			= '비밀번호 찾기';
$lang['email']				= '이메일 주소';

//index_body.tpl
$lang['user']				= '이름';
$lang['pass']				= '비밀번호';
$lang['remember_pass']			= '비밀번호 기억';
$lang['lostpassword']			= '혹시 비밀번호를 잊어버리셨나요?';
$lang['welcome_to']			= '반갑습니다! 여긴 ';
$lang['server_description']		= '</strong> is a <strong>strategic space simulation game</strong> with <strong>thousands of players</strong> all over the world competing <strong>simultaneously</strong> to be the best. 현재 함속 20배 자원 20배입니다. 게임을 즐기기 위해선 최신의 웹 브라우저가 필요합니다. 한글화에 도움을 주신 분들: 튜햄, 소유. 감사합니다!';
$lang['server_register']		= '가입하기!';
$lang['server_message']			= 'Join now and be part of the incredible world of';
$lang['login']				= '로그인';

/* ------------------------------------------------------------------------------------------ */

//reg.php
$lang['reg_mail_text_part1']		= 'Thank you for signing up for our game. \n Your password is: ';
$lang['reg_mail_text_part2']		= ' \n\n Enjoy the game! \n ';
$lang['register_at']			= 'Register at ';
$lang['reg_mail_message_pass']		= 'Thank you for signing up!';
$lang['invalid_mail_adress']		= 'Invalid Email!<br />';
$lang['empty_user_field']		= 'The user field can not be empty!<br />';
$lang['password_lenght_error']		= 'The password must be at least 4 characters!<br />';
$lang['user_field_no_alphanumeric']	= 'The user field can contain only alphanumeric characters!<br />';
$lang['terms_and_conditions']		= 'You must accept our terms and conditions of use!<br />';
$lang['user_already_exists']		= 'The chosen user name already exists!<br />';
$lang['mail_already_exists']		= 'The email entered already exists!<br />';
$lang['welcome_message_from']		= 'Admin';
$lang['welcome_message_sender']		= 'Admin';
$lang['welcome_message_subject']	= 'Welcome';
$lang['welcome_message_content']	= 'Welcome to XG project!<p>At the beginning, build a metal mine.<br />To do this, click on the link "buildings" on the left, and click on "build " to the right of mine metal.<br />Now you have some time to learn more about the game.<p>Can find help:<br />In our <a href=\"http://www.xtreme-gamez.com.ar/foros\" target=\"_blank\">Forums</a><br />Now your mine should be finished.<br />As produce nothing without energy, you should build a solar power plant, re-building, and chose to build solar power plants.<p>To see all the ships, defense structures, buildings and research that can investigate, you can have a look at tree tech "Technology" in the left menu.<p>You can now begin the conquest of the universe ... Good luck!';
$lang['newpass_smtp_email_error']	= '<br><br>An error occurred in sending the e-mail. Your password is: ';
$lang['reg_completed']			= 'Register complete!';

//registry_form.tpl
$lang['server_message_reg']		= 'Join now and be part of the incredible world of';
$lang['register_at_reg']		= 'Register at';
$lang['user_reg']			= 'User';
$lang['pass_reg']			= 'Password';
$lang['email_reg']			= 'Email adress';
$lang['register_now']			= 'Sign Up!';
$lang['accept_terms_and_conditions']	= 'Accept terms and conditions of use';

?>
